## JS SDK 接入指南

### web 版
将以下JS代码复制到您所需分析页面中的 ```<head>``` 和 ```</head>``` 标签之间即可。  
```
<script type='text/javascript'>
    var _ejs = _ejs || [];
    window._ejs = _ejs;
    (function () {
        _ejs.push(['appId', string]);//（必填）系统分配给用户的项目标识
        _ejs.push(['wt', '0']); //（必填）类型：0官网 1推广链接
        _ejs.push(['env','prd']);//日志环境：dev（上报测试环境）  默认prd（上报生产环境） 
        _ejs.push(['platform','web']);//SDK模式：client（应用版） 默认web（网页版）  
        _ejs.push(['d',string]);//用户id 默认为空   
        _ejs.push(['batch',Number]);//批量发送报文的个数：默认1（实时上报） 
        (function () {
            var vds = document.createElement('script');
            vds.type='text/javascript';
            vds.async = true;
            //sdk地址
            vds.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'analysis.hiwemeet.com/web/cysdk.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(vds, s);
        })();
    })();
</script>
```

#### 手动埋点
1. 接口方法：```Cysdk.setEventU(param)```
2. 参数描述：
            ```param = {
                eventId: string, // 事件ID，用户自定义
                href: string, // 标签内的跳转链接（href）
                expand: mappingObj // 扩展字段，{key1:value1,key2:value2} 根据需求进行扩展
            };```
3. 使用示例（购买按钮）：
```
<script type='text/javascript'> 
 param = {
     eventId: 'buy',
     href: './buy.html',
     expand: {order_id: 545678}
 };
Cysdk.setEventU(param);
</script>
```
4. 手动埋点用户标识 ```(d)uid```
```
<script type='text/javascript'>
//手动埋点，JS添加如下代码, d 为手动设置的用户ID
var param = {
     eventId: string,//事件id
     d: string,//用户ID
     expand:mapping//自定义数据字典{key:value...}
};
//防止SDK载入失败会对业务逻辑造成影响，可以增加此判断
if(window.Cysdk)
    Cysdk.setEventU(param);

//例：在用户点击退出按钮后，JS添加如下代码, d 为空字符串
var param = {
    eventId: "logout",
    d: "xiaoming666",
    expand:{
        "age":18
    }
};
if(window.Cysdk)
    Cysdk.setEventU(param);

</script>
```

### APP 版
应用接入Android SDK和iOS SDK后，js SDK会自动注入到应用的h5中。