## JS SDK 接入指南

### PC 版

将以下JS代码复制到您所需分析页面中的 <head> 和 </head> 标签之间即可。  
```
<script type='text/javascript'>  
        var _ejs = _ejs || [];
        window._ejs = _ejs;
        (function(){  
            (function() {
                window.dev = false; //线上模式false, 若需调试测试环境设置为true
                _ejs.push(['appId', 'xxxxx']); //系统分配给用户的项目标识, 库拍项目ID  985477585
                _ejs.push(['wt', '1']); // 用户自定义值       
                var vds = document.createElement('script');
                vds.type='text/javascript';
                vds.async = true;
                vds.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'analysis.hiwemeet.com/web/cw.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(vds, s);
            })();
        })();
    </script>
```

#### 手动埋点使用说明
1. 接口方法：```setEventU(param)```
2. 参数描述：
            ```param = {
                eventId: 'buy', // 事件ID，用户自定义
                href: '', // 标签内的跳转链接（href）
                expand: {order_id: 545678} // 扩展字段，{key1:value1,key2:value2} 根据需求进行扩展
            };```
3. 使用示例（购买按钮）：
```
<script type='text/javascript'> 
 param = {
     eventId: 'buy',
     href: '',
     expand: {order_id: 545678}
 };
setEventU(param)
</script>
```
4. 手动埋点用户标识 uid
```
<script type='text/javascript'>
//在用户点击登录按钮后，JS添加如下代码, accountID 为用户传进来的值
param = {
     eventId: 'login',
     uid: 'accountID'
 };
 //防止SDK载入失败会对业务逻辑造成影响，可以增加此判断
 if(window.setEventU)
    setEventU(param)
    
//在用户点击退出按钮后，JS添加如下代码, accountID 为空
param = {
     eventId: 'logOut',
     uid: ''
 };
 //防止SDK载入失败会对业务逻辑造成影响，可以增加此判断
 if(window.setEventU)
    setEventU(param)
</script>
```

### Mobile 版

将以下JS代码复制到您所需分析页面中的 <head> 和 </head> 标签之间即可。

```
<script type='text/javascript'>
        (function(){
            (function() {
                window.dev = true; //测试模式打开
                var vds = document.createElement('script');
                vds.type='text/javascript';
                vds.async = true;
                vds.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'analysis.hiwemeet.com/web/cm.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(vds, s);
            })();
        })();
</script>
```