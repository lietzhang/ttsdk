var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var browserify = require('gulp-browserify');

var taskMap = ['v1300','v1200','v1112','v1111','v1100','v1003'];
var ver = '';
for(var i in taskMap){
    if(taskMap.hasOwnProperty(i)){
        ver = taskMap[i];
    }else{
        continue;
    }
    (function (ver) {
        gulp.task(ver, [], function () {
            gulp.src( 'scripts/crawler' + ver.slice(1) + '.js')
                .pipe(browserify())
                .pipe(uglify())
                .pipe(concat('cysdk-' + ver.slice(1) + '.min.js'))
                .pipe(gulp.dest('dist/cysdk'));
        });
    })(ver)
}

gulp.task('watch',[],function () {
    gulp.watch('scripts/*.js',['default']);
});

gulp.task('default', taskMap);