(function(){

    window.Class=arguments.callee;
    Class.a=123123
})();(function(factory){
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return factory(window)
        })
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(window)
    }else{
        window._fp = factory(window)
    }
})(function (window, customStr) {
    var navigator = window.navigator;
    var screen = window.screen;
    var document = window.document;
    customStr = customStr ? customStr : "";

    // function bin2hex(s) {
    //     var i, l, o = '',
    //         n;
    //
    //     s += '';
    //
    //     for (i = 0, l = s.length; i < l; i++) {
    //         n = s.charCodeAt(i)
    //             .toString(16);
    //         o += n.length < 2 ? '0' + n : n;
    //     }
    //
    //     return o;
    // }

    function getGPU() {
        var canvas = document.createElement('canvas');
        return canvas.toDataURL().replace("data:image/png;base64,", "");

        // var gpu = canvas.getContext("webgl");
        // if(gpu.__proto__){
        //     var gpuInfo = "";
        //     for(var i in gpu.__proto__){
        //         if(gpu.__proto__.hasOwnProperty(i)) {
        //             try{
        //                 gpuInfo += gpu.__proto__[i];
        //             }catch (e){
        //                 break;
        //             }
        //         }
        //     }
        //     return gpuInfo;
        // }else{
        //     return gpu.getParameter(gpu.VERSION)
        // }
    }
    function getDeviceInfoHash() {
        var deviceInfo = '';
        var userAgent = navigator.userAgent.toLowerCase();
        var os = 'pc';
        if (userAgent.indexOf('android') > -1 || userAgent.indexOf('linux') > -1) {//安卓手机
            os = 'android';
            var _dev = userAgent.match(/\((.*?)\)/)[0].replace(/[\(\)\s]/g,'').replace(/(;zh\-cn;?)|(;u;?)|(;wv;?)/ig,';').replace(/;$/,'');
            _dev = _dev.replace(/;/g,'_');
            //搜狗浏览器UA会有4断
            if(_dev.match(/_/g).length > 2){
                var i = _dev.lastIndexOf("_");
                _dev = _dev.substr(0,i) + _dev.substr(i+1)
            }
            //魅族标识进行统一
            if(_dev.search('_mz-') > -1){
                _dev = _dev.replace('_mz-','_')
            }
            os += _dev;
        } else if (userAgent.indexOf('iphone') > -1) {//苹果手机
            os = 'ios';
            os += userAgent.match(/cpu iphone os (.+) like mac os/)[1];
        } else if (userAgent.indexOf('windows phone') > -1) {//winphone手机
            os = 'windows';
        }
        deviceInfo += os;
        // deviceInfo += navigator.language; //浏览器支持的语言
        // deviceInfo += navigator.product; // 浏览器的产品名
        // deviceInfo += navigator.productSub; //关于浏览器更多信息
        // deviceInfo += navigator.vender; // 浏览器厂商名称
        // deviceInfo += navigator.vendorSub; // 关于浏览器厂商更多的信息
        // for(var idx in navigator.plugins){
        //     deviceInfo += navigator.plugins[idx].name;
        // }
        // deviceInfo += screen.pixelDepth;
        // deviceInfo += screen.colorDepth;
        // deviceInfo += screen.height;
        // deviceInfo += screen.width;
        return deviceInfo.toLowerCase().replace(/\s/g, "");
    }
    return getGPU() + getDeviceInfoHash() + customStr
});
