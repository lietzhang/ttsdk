(function (window) {
    //防止重复加载
    if(window.webviewJSbridge || window.Cysdk) return false;
    var md5 = require('./libs/md5');
    var pako = require('./libs/pako_deflate');
    //sdk全局对象
    var jsBridge = {};
    var VERSION = '1.1.1.2';//sdk版本号

    var S2_TIME = 30 * 60 * 1000;//有效期30分钟
    var CO_TIME = 3 * 365 * 24 * 60 * 60 * 1000;// 有效期三年
    // var GEO_INFO = {};  //地理位置信息
    var FW_STATE = 'uninstall';
    //全局默认配置
    var globalObject = {
        env : 'prd',//dev|prd 开发模式 生产模式
        platform : 'web',//client|web 控制日志参数格式
        selection: false//js 圈选开关
    };
    //获取当前页面的参数信息
    var location = {
        absUrl: window.location.href,
        referUrl: document.referrer,
        domain: window.location.hostname,
        port: window.location.port,
        path: window.location.pathname,
        search: window.location.search ? window.location.search.substring(1) : '',
        hash: window.location.hash.slice(1),
        title: document.title,
        userAgent: navigator.userAgent
    };
    //
    // function Location(){
    //     return util.extend(this,{
    //         absUrl: window.location.href,
    //         referUrl: document.referrer,
    //         domain: window.location.hostname,
    //         port: window.location.port,
    //         path: window.location.pathname,
    //         search: window.location.search ? window.location.search.substring(1) : '',
    //         hash: window.location.hash,
    //         title: document.title,//document.getElementsByTagName("title")[0].innerHTML,
    //         userAgent: navigator.userAgent
    //     })
    // }
    // Location.prototype.getTitle = function(){
    //     return this.title;
    // }

    // api接口 分别对应https or http
    var reportApi = ('https:' === document.location.protocol ? 'https://sdkstatic.51kupai.com/' : 'http://sdkstatic.hiwemeet.com/');
    // 开发环境设置
    var envConfig = {
        dev: {
            debug: true,
            api: reportApi + 'sdkoff'
        },
        prd: {
            debug: false,
            api: reportApi + 'sdkon'
        }
    };
    // 开发环境，打印日志
    var logger = {
        // object to string
        c: function (obj,start,end) {
            if (globalObject.env !== 'dev') return false;
            if(start){
                console.group(obj);
            }else{
                console.log(obj);
            }
            if(end){
                console.groupEnd();
            }
        }
    };

    // cookie读写
    var cookies = {
        set: function (key, expTime) {
            var exp = new Date();
            exp.setTime(exp.getTime() + expTime);
            //var random = parseInt(Math.random() * 1000000);
            var random = util.generateMixed(6);
            var value = exp.getTime() + '_' + random;
            document.cookie = key + '=' + value + ';expires=' + exp.toGMTString() + ';path=/';
        },
        /**
         * @desc Get the value of given cookie key
         * @param key {String} Id to use for lookup
         * @return {String} Return the key of value
         */
        get: function (key) {
            //
            var arr, reg = new RegExp("(^| )" + key + "=([^;]*)(;|$)");
            if (arr = document.cookie.match(reg)) return unescape(arr[2]);
            else return '';
        },
        /**
         * @desc Remove given cookie
         * @return void
         */
        remove: function (key) {
            this.set(key, -1);
        },

        isCookieExisted: function (key) {
            var val = this.get(key);
            var ret = false;
            if (!!val) {
                ret = true;
            }
            return ret;
        }
    };

    // event listener function
    var eventListener = {
        /**
         * @desc add listener IE and chrome
         * @param node {Object} document node element
         * @param type {String} event type name
         * @param handler {Function} callback method
         * @return {Boolean} true or false
         */
        addEvent: function (node, type, handler) {
            if (!node) return false;
            if (node.addEventListener) {
                node.addEventListener(type, handler, false); // 所有主流浏览器，除了 IE 8 及更早 IE版本
                return true;
            }
            else if (node.attachEvent) {
                node['e' + type + handler] = handler;
                node[type + handler] = function () {
                    node['e' + type + handler](window.event);
                };
                node.attachEvent('on' + type, node[type + handler]); // IE 8 及更早 IE 版本
                return true;
            }
            return false;
        },
        /**
         * @desc remove listener IE and chrome
         * @param node {Object} document node element
         * @param type {String} event type name
         * @param handler {Function} callback method
         * @return {Boolean} true or false
         */
        removeEvent: function (node, type, handler) {
            if (!node) return false;
            if (node.removeEventListener) {
                node.removeEventListener(type, handler, false);
                return true;
            }
            else if (node.detachEvent) {
                node.detachEvent('on' + type, node[type + handler]);
                node[type + handler] = null;
            }
            return false;
        },
        /**
         * @desc mobile touch event
         * @param obj {Object} document node element
         * @param type {String} event type name
         * @param func {Function} callback method
         * @return void
         */
        touchEvent: function (obj, type, func) {
            //滑动范围在5x5内则做点击处理，s是开始，e是结束
            var init = {x: 5, y: 5, sx: 0, sy: 0, ex: 0, ey: 0};
            var sTime = 0, eTime = 0;
            type = type.toLowerCase();

            obj.addEventListener("touchstart", function () {
                sTime = new Date().getTime();
                init.sx = event.targetTouches[0].pageX;
                init.sy = event.targetTouches[0].pageY;
                init.ex = init.sx;
                init.ey = init.sy;
                if (type.indexOf("start") !== -1) func();
            }, false);

            obj.addEventListener("touchmove", function () {
                event.preventDefault();//阻止触摸时浏览器的缩放、滚动条滚动
                init.ex = event.targetTouches[0].pageX;
                init.ey = event.targetTouches[0].pageY;
                if (type.indexOf("move") !== -1) func();
            }, false);

            obj.addEventListener("touchend", function () {
                var changeX = init.sx - init.ex;
                var changeY = init.sy - init.ey;
                if (Math.abs(changeX) > Math.abs(changeY) && Math.abs(changeY) > init.y) {
                    //左右事件
                    if (changeX > 0) {
                        if (type.indexOf("left") !== -1) func();
                    } else {
                        if (type.indexOf("right") !== -1) func();
                    }
                }
                else if (Math.abs(changeY) > Math.abs(changeX) && Math.abs(changeX) > init.x) {
                    //上下事件
                    if (changeY > 0) {
                        if (type.indexOf("top") !== -1) func();
                    } else {
                        if (type.indexOf("down") !== -1) func();
                    }
                }
                else if (Math.abs(changeX) < init.x && Math.abs(changeY) < init.y) {
                    eTime = new Date().getTime();
                    //点击事件，此处根据时间差细分下
                    if ((eTime - sTime) > 300) {
                        if (type.indexOf("long") !== -1) func(); //长按
                    }
                    else {
                        if (type.indexOf("click") !== -1) func(); //当点击处理
                    }
                }
                if (type.indexOf("end") !== -1) func();
            }, false);
        }
    };

    //JS http ajax request
    var httpRequest = {
        /**
         * @desc ajax http request
         * @param type {String} post get put delete method
         * @param url {String} api url
         * @param data {Object} post data, get method without this param
         * @param success {Function} callback when success
         * @param failed {Function} callback when failed
         * @return void
         */
        ajax: function (type, url, data, success, failed) {
            // 创建ajax对象
            var xhr = null;
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else {
                xhr = new ActiveXObject('Microsoft.XMLHTTP');
            }

            type = type.toUpperCase();
            // 用于清除缓存
            var random = Math.random();

            if (typeof data === 'object') {
                var str = '';
                for (var key in data) {
                    str += key + '=' + data[key] + '&';
                }
                data = str.replace(/&$/, '');
            }

            if (type === 'GET') {
                if (data) {
                    xhr.open('GET', url + '?' + data, true);
                } else {
                    xhr.open('GET', url + '?t=' + random, true);
                }
                xhr.send();

            } else if (type === 'POST') {
                xhr.open('POST', url, true);
                // 如果需要像 html 表单那样 POST 数据，请使用 setRequestHeader() 来添加 http 头。
                xhr.setRequestHeader("Accept-Encoding-SDK","sdk_gzip");
                // xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                // xhr.send(data);
                // console.log(pakoin.ungzip(pako.gzip(data),{to:'string'}))
                xhr.send(pako.gzip(data));
            }

            // 处理返回数据
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        success(xhr.responseText);
                    } else {
                        if (failed) {
                            failed(xhr.status);
                        }
                    }
                }
            }
        },
        /**
         * @desc load js script dynamically
         * @param src {src} api url
         * @param callback {Function} callback when success
         * @return void
         */
        loadScript: function (src, callback) {
            var script = document.createElement('script'),
                head = document.getElementsByTagName('head')[0];
            script.type = 'text/javascript';
            script.charset = 'UTF-8';
            script.src = src;
            if (script.addEventListener) {
                script.addEventListener('load', function () {
                    callback();
                }, false);
            } else if (script.attachEvent) {
                script.attachEvent('onreadystatechange', function () {
                    var target = window.event.srcElement;
                    if (target.readyState === 'loaded') {
                        callback();
                    }
                });
            }
            head.appendChild(script);
        }
    };

    // geo information
    // var geoInfo = {
    //     location: function () {
    //         httpRequest.loadScript('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=jsonp', function () {
    //             var obj = {
    //                 country: remote_ip_info.country,
    //                 province: remote_ip_info.province,
    //                 city: remote_ip_info.city
    //             };
    //             GEO_INFO.country = obj.country;
    //             GEO_INFO.province = obj.province;
    //             GEO_INFO.city = obj.city;
    //             logger.c(obj);
    //         });
    //     },
    //     ipAddress: function () {
    //         httpRequest.loadScript('http://www.coding123.net/getip.ashx?js=1', function (ip) {
    //             var obj = {
    //                 ipv4: ip.substr(1)
    //             };
    //             GEO_INFO.ipv4 = obj.ipv4;
    //             logger.c(obj);
    //         });
    //     }
    // };

    //获取当前点击时间
    var dateTime = {
        formatTime: function () {
//
            Date.prototype.Format = function (fmt) { //author: meizz
                var o = {
                    "M+": this.getMonth() + 1, //月份
                    "d+": this.getDate(), //日
                    "h+": this.getHours(), //小时
                    "m+": this.getMinutes(), //分
                    "s+": this.getSeconds(), //秒
                    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                    "S": this.getMilliseconds() //毫秒
                };
                if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
                for (var k in o)
                    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
                return fmt;
            };

            //var time1 = new Date().Format("yyyy-MM-dd");
            return new Date().Format("yyyy-MM-dd hh:mm:ss");
        },
        unixTime: function () {
            return new Date().getTime();
        }
    };


    // 设备操作系统判断
    var deviceDetector = {
        //判断当前浏览器的类型
        browser: function () {
            var OsObject = {};
            if (navigator.userAgent.indexOf("MSIE") > 0) {
                OsObject = {browser: 'IE'};
            }
            if (navigator.userAgent.indexOf("Chrome") > 0) {
                if (navigator.userAgent.indexOf("OPR/") > 0) {
                    OsObject = {browser: 'Opera'};
                } else if (navigator.userAgent.indexOf("BIDUBrowser") > 0) {
                    OsObject = {browser: 'baidu'};
                } else if (navigator.userAgent.indexOf("UBrowser") > 0) {
                    OsObject = {browser: 'UC'};
                } else if (navigator.userAgent.indexOf("QQBrowser") > 0) {
                    OsObject = {browser: 'QQBrowser'};
                } else if (navigator.userAgent.indexOf("2345Explorer") > 0) {
                    OsObject = {browser: '2345Explorer'};
                } else {
                    OsObject = {browser: 'Chrome'};
                }
            } else if (navigator.userAgent.indexOf("Safari") > 0) {
                OsObject = {browser: 'Safari'};
            }
            if (navigator.userAgent.indexOf("Firefox") > 0) {
                OsObject = {browser: 'Firefox'};
            }
            return OsObject;
        },
        os: function () {
            var osData = {};
            var sUserAgent = navigator.userAgent;
            var isWin = (navigator.platform === "Win32") || (navigator.platform === "Windows");
            var isMac = (navigator.platform === "Mac68K") || (navigator.platform === "MacPPC") || (navigator.platform === "Macintosh") || (navigator.platform === "MacIntel");
            if (isMac) {
                osData.os = 'macOS';
            }
            var isUnix = (navigator.platform === "X11") && !isWin && !isMac;
            if (isUnix) {
                osData.os = 'Unix';
            }
            var isLinux = (String(navigator.platform).indexOf("Linux") > -1);
            if (isLinux) {
                osData.os = 'Linux';
            }
            if (isWin) {
                var isWin2K = sUserAgent.indexOf("Windows NT 5.0") > -1 || sUserAgent.indexOf("Windows 2000") > -1;
                if (isWin2K) {
                    osData.os = 'Win2000';
                }
                var isWinXP = sUserAgent.indexOf("Windows NT 5.1") > -1 || sUserAgent.indexOf("Windows XP") > -1;
                if (isWinXP) {
                    osData.os = 'WinXP';
                }
                var isWin2003 = sUserAgent.indexOf("Windows NT 5.2") > -1 || sUserAgent.indexOf("Windows 2003") > -1;
                if (isWin2003) {
                    osData.os = 'Win2003';
                }
                var isWinVista = sUserAgent.indexOf("Windows NT 6.0") > -1 || sUserAgent.indexOf("Windows Vista") > -1;
                if (isWinVista) {
                    osData.os = 'WinVista';
                }
                var isWin7 = sUserAgent.indexOf("Windows NT 6.1") > -1 || sUserAgent.indexOf("Windows 7") > -1;
                if (isWin7) {
                    osData.os = 'Win7';
                }
                var isWin8 = sUserAgent.indexOf("Windows NT 8.1") > -1 || sUserAgent.indexOf("Windows 8") > -1;
                if (isWin8) {
                    osData.os = 'Win8';
                }
                var isWin10 = sUserAgent.indexOf("Windows NT 10.0") > -1 || sUserAgent.indexOf("Windows 10") > -1;
                if (isWin10) {
                    osData.os = 'Win10';
                }
            }
            if (sUserAgent.indexOf('Android') > -1 || sUserAgent.indexOf('Linux') > -1) {//安卓手机
                osData.os = 'Android';
            } else if (sUserAgent.indexOf('iPhone') > -1) {//苹果手机
                osData.os = 'iOS';
            } else if (sUserAgent.indexOf('Windows Phone') > -1) {//winphone手机
                osData.os = 'winPhone';
            }
            return osData;
        }
    };

    //工具函数
    var util = {
        /*
        * 过滤文本中的任何空白字符，包括空格、制表符、换页符等等。等价于[ \f\n\r\t\v]。
        * */
        trim:function (str) {
            return str.replace(/\s/g,'');
        },
        /*
        * 获取滚动轴位置
        * */
        scroll:function(name){
            var elem = document;
            var doc = elem.documentElement;

            // Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height], whichever is greatest
            // unfortunately, this causes bug #3838 in IE6/8 only, but there is currently no good, small way to fix it.
            return Math.max(elem.body["scroll" + name], doc["scroll" + name], elem.body["offset" + name], doc["offset" + name], doc["client" + name]);
        },
        /*
        * 获取当前EL的样式
        * */
        css:function(el,style){
            if(el.currentStyle) {
                return el.currentStyle[style];
            } else if(window.getComputedStyle) {
                return window.getComputedStyle(el , null)[style];
            }
        },
        /**
         * Is #obj empty?
         * @param {Object} obj object to be detected.
         */
        isEmptyObject: function (obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    return false;
                }
            }
            return true;
        },

        /**
         * @desc  获取随机固定位数的随机字符串
         * @param length {Number} 返回的随机字符串位数
         * @return {String} 随机字符串
         */
        generateMixed: function (length) {
            var chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
            var res = "";
            for (var i = 0; i < length; i++) {
                var id = Math.ceil(Math.random() * 35);
                res += chars[id];
            }
            return res;
        },
        queryString: function () {
            // This function is anonymous, is executed immediately and
            // the return value is assigned to QueryString!
            var query_string = {};
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                // If first entry with this name
                if (typeof query_string[pair[0]] === "undefined") {
                    query_string[pair[0]] = decodeURIComponent(pair[1]);
                    // If second entry with this name
                } else if (typeof query_string[pair[0]] === "string") {
                    query_string[pair[0]] = [query_string[pair[0]], decodeURIComponent(pair[1])];
                    // If third or later entry with this name
                } else {
                    query_string[pair[0]].push(decodeURIComponent(pair[1]));
                }
            }
            return query_string;
        },

        getXPath: function (element, flag) {
            if (element === undefined || element.nodeName === '#document' || element.tagName.toLocaleLowerCase() === 'html') {
                return '';
            }
            var parentElement = element.parentNode ? element.parentNode : element.parentElement;
            var eleList = parentElement.getElementsByTagName(element.tagName);
            var obj = {};
            for (var i = 0; i < eleList.length; i++) {
                if (eleList[i] === element) {
                    obj.tagName = element.tagName.toLocaleLowerCase();
                    obj.index = i;
                    obj.id = element.id;
                    obj.className = element.className;
                    break;
                }
            }
            var str = obj.tagName;
            var path = util.getXPath(parentElement, true) + str + '[' + obj.index + ']';
            if (flag) { //当前元素的末尾不加'/'
                path += '/';
            }
            return path;
        },

        getAllXpath: function (xpath) {
            var ret = [];
            var arr = [];
            var currentXpath = '';
            var list = xpath.split('/');
            for (var i = 0; i < list.length; i++) {
                if (!list[i]) return;
                arr.push(list[i]);
                currentXpath = arr.join('/');
                ret.push(md5(currentXpath));
                //ret.push(currentXpath);
            }
            // 保留三层
            //return ret.slice(ret.length - 3, ret.length);
            return ret;
        },

        // 保留三层 Xpath
        get3LayersXpath: function (allXpathArr) {
            var ret = allXpathArr || [];
            if (allXpathArr.length > 3) {
                ret = allXpathArr.slice(allXpathArr.length - 3, allXpathArr.length);
            }
            return ret;
        },
        //控件类型 Map
        elementTypeMap: function (tagName, type) {
            if (type) {
                tagName = tagName + '-' + type.toLocaleLowerCase();
            }
            tagName = tagName.toLocaleLowerCase();
            var ret = 12; //自定义
            switch (tagName) {
                case 'button-submit':
                case 'input-button':
                case 'input-submit':
                case 'a':
                    ret = 1;
                    break;
                case 'label':
                case 'p':
                    ret = 2;
                    break;
                case 'img':
                    ret = 3;
                    break;
                case 'input-text':
                case 'textarea-textarea':
                case 'input-number':
                case 'input-email':
                case 'input-phone':
                    ret = 4;
                    break;
                case 'input-search':
                    ret = 5;
                    break;
                case 'li':
                case 'dt':
                case 'dd':
                    ret = 6;
                    break;
                case 'td':
                    ret = 7;
                    break;
                case 'input-radio':
                    ret = 8;
                    break;
                case 'input-checkbox':
                    ret = 9;
                    break;
                case 'select':
                    ret = 15;
                    break;
                case 'video':
                    ret = 16;
                    break;
                case 'audio':
                    ret = 17;
                    break;
                case 'canvas':
                    ret = 18;
                    break;
            }
            return ret;
        },
        extend:function (obja, objb) {
            if(!this.isObject(obja)){
                return objb;
            }else if(!this.isObject(objb)){
                return obja;
            }else{
                for(var i in objb){
                    if(objb.hasOwnProperty(i)){
                        if(objb[i] === undefined) continue;
                        obja[i] = objb[i];
                    }
                }
                return obja;
            }
        },
        isObject:function(val){
            return val.constructor === Object
        },
        /*
        *
        * */
        encodeURIExceptProtocol:function (url) {
            var len = 0;
            if(url.search(/^https/) !== -1){
                len = 8;
            }else if(url.search(/^http/) !== -1){
                len = 7;
            }
            return encodeURIComponent(url.slice(len));
        }
    };

    /*-------------- Business Logic ---------------------------------------------------------------------------------*/
    //圈选类
    function SelectionCircle(){
        var wrap = document.createElement('div');
        wrap.style.border = "2px solid #ed495f";
        wrap.style.boxShadow = "0 2px 8px rgba(0, 0, 0, 0.8)";
        wrap.style.boxSizing = "border-box";
        wrap.style.position = "absolute";
        wrap.style.zIndex = "9999";
        wrap.style.transition = "all 0.5s";
        wrap.classList.add('cw-selection-circle');
        this.el = wrap;
        this.ret = [9999,9999,0,0];
        this.update.apply(this,this.ret);
        document.body.appendChild(this.el);
    }
    SelectionCircle.prototype.update = function(x,y,w,h){
        if(this.ret.join(',') === Array.prototype.join.call(arguments,',')){
            return false;
        }
        this.ret = [x,y,w,h];
        this.el.style.left = x + 'px';
        this.el.style.top = y + 'px';
        this.el.style.width = w + 'px';
        this.el.style.height = h + 'px';
    };
    SelectionCircle.prototype.draw = function(e){
        var x = e.clientX !== undefined ? e.clientX : e.targetTouches[0].clientX;
        var y = e.clientY !== undefined ? e.clientY : e.targetTouches[0].clientY;
        var _ret = jsBridge.getTargetElementForMove(x, y);
        _ret = JSON.parse(_ret);
        this.update(_ret.x + util.scroll('Left'),_ret.y + util.scroll('Top'),_ret.w,_ret.h);
    };

    var addListenerForTags = function () {
        var handlerWebCrawler = function (event) {
            //IE only support srcElement, firefox only support target
            var eventTarget = event.srcElement ? event.srcElement : event.target;
            var obj = {
                innerHTML: util.trim(eventTarget.innerHTML), // contains html tags
                innerText: util.trim(eventTarget.innerText), // only text content
                clientX: event.clientX || event.targetTouches[0].clientX,
                clientY: event.clientY || event.targetTouches[0].clientY,
                screenX: event.screenX || event.targetTouches[0].screenX,
                screenY: event.screenY || event.targetTouches[0].screenY,
                eventType: event.type, // event type, e.g. click, touchstart etc.
                tagName: eventTarget.tagName, // html tags
                title: eventTarget.title,
                id: eventTarget.id,
                className: eventTarget.className,
                baseURI: eventTarget.baseURI, // absolute url
                parentNode: eventTarget.parentNode, // parent dom element
                attributes: eventTarget.attributes, // all of attributes with current html tag, {Array}
                path: event.path // all of parent node dom elements, {Array},
            };

            // handler path node
            var pathArr = [];
            if (obj.path) {
                for (var i = 0; i < obj.path.length; i++) {
                    var pathObj = {};
                    var pathEventObj = obj.path[i];
                    if (pathEventObj.tagName) {
                        pathObj.tagName = pathEventObj.tagName;
                        if (!util.isEmptyObject(pathObj)) {
                            pathArr.push(pathObj);
                        }
                    }
                }
            }
            obj.path = pathArr;

            // get xpath td[@id='tableEnd' and @class='table tr5 td4'][3]/tr[4]/tbody[0]/table[0]/div[2]/
            obj.xpath = util.getXPath(eventTarget);

            // form input
            if ('input' === eventTarget.tagName.toLocaleLowerCase()) {
                obj.inputType = eventTarget.type; // for input tag, type attr
                obj.value = eventTarget.value; // for input tag, type attr
                obj.name = eventTarget.name; // for input tag, type attr
            }

            // select
            if ('select' === eventTarget.tagName.toLocaleLowerCase()) {
                obj.selectedIndex = eventTarget.selectedIndex;
                var selectedOptions = [];
                var arrays = eventTarget.selectedOptions;
                for (var j = 0; j < arrays.length; j++) {
                    var o = {
                        innerHTML: util.trim(arrays[j].innerHTML),
                        tagName: arrays[j].tagName,
                        value: arrays[j].value,
                        index: arrays[j].index
                    };
                    selectedOptions.push(o);
                }
                obj.selectedOptions = selectedOptions;
            }
            // table
            if ('td' === eventTarget.tagName.toLocaleLowerCase() ||
                'li' === eventTarget.tagName.toLocaleLowerCase()) {
                obj.rowIndex = eventTarget.parentNode.rowIndex; //当前单元格行值
                obj.cellIndex = eventTarget.cellIndex; //当前单元格列值
                obj.rows = eventTarget.parentNode.parentNode.childElementCount; //该表总行数
                obj.columns = eventTarget.parentNode.childElementCount; //该表总列数
            }
            // img
            if ('img' === eventTarget.tagName.toLocaleLowerCase()) {
                obj.width = eventTarget.width;
                obj.height = eventTarget.height;
                obj.alt = eventTarget.alt;
                obj.src = eventTarget.src;
            }
            // a link
            if ('a' === eventTarget.tagName.toLocaleLowerCase()) {
                obj.href = eventTarget.href;
                obj.rel = eventTarget.rel;
                obj.target = eventTarget.target;
            }
            //父级
            var parentElement = eventTarget.parentNode;
            if(parentElement && parentElement.nodeName !== '#document' && parentElement.nodeName.toLocaleLowerCase() !== 'html'){
                var pBound = parentElement.getBoundingClientRect();
                obj.pXpath = util.getXPath(parentElement);
                obj.pInnerHTML = util.trim(parentElement.innerHTML);
                obj.pInnerText = util.trim(parentElement.innerText);
                obj.pClientX = pBound.left;
                obj.pClientY = pBound.top;
                obj.pTagName = parentElement.tagName;
                obj.pInputType = parentElement.type;
                obj.pHref = parentElement.href;

                //祖父级
                var grandpaElement = parentElement.parentNode;
                if(grandpaElement && grandpaElement.nodeName !== '#document' && grandpaElement.nodeName.toLocaleLowerCase() !== 'html') {
                    var gBound = grandpaElement.getBoundingClientRect();
                    obj.gpXpath = util.getXPath(grandpaElement);
                    obj.gpInnerHTML = util.trim(grandpaElement.innerHTML);
                    obj.gpInnerText = util.trim(grandpaElement.innerText);
                    obj.gpClientX = gBound.left;
                    obj.gpClientY = gBound.top;
                    obj.gpTagName = grandpaElement.tagName;
                    obj.gpInputType = grandpaElement.type;
                    obj.gpHref = grandpaElement.href;
                }
            }
            sendRequest(obj);
        };

        var handlerWebSelection = function (event, selectionCircle) {
            //IE only support srcElement, firefox only support target
            selectionCircle.draw(event);
            //阻止链接的的默认行为
            // event.preventDefault();
            return false;
        };
        var sendRequest = function (object) {
            //data format by requirement
            // event type map
            if (!cookies.isCookieExisted('s2')) {
                cookies.set('s2', S2_TIME);
            }
            if (!cookies.isCookieExisted('co')) {
                cookies.set('co', CO_TIME);
            }
            var osInfo = deviceDetector.os();
            var xpathArr = util.get3LayersXpath(util.getAllXpath(object.xpath));
            var eventId = md5(object.xpath); //event ID format: path_text
            if (object.innerText) {
                eventId = md5(object.xpath + '_' + object.innerText);
            }

            var dataObj = {};
            // 客户端日志规范
            dataObj.client = {
                client_event: [
                    {
                        v: eventId,
                        a: clientOptions.appId || globalObject.appId || '', // app ID
                        b: clientOptions.b || globalObject.b || '', // app version
                        c: clientOptions.c || globalObject.c || '', // channel ID
                        d: clientOptions.d || globalObject.d || '', // uid
                        e: clientOptions.e || globalObject.e || '', // device ID
                        g: clientOptions.g,  // 操作系统
                        j: clientOptions.j || globalObject.j || '',  // 广告标识符
                        t1: dateTime.unixTime(), //进入p1客户端时间
                        w: eventTypeMap(object.eventType), //option type by user
                        x1: object.clientX,
                        y1: object.clientY,
                        y: object.innerText || '', //contains html text
                        y2: object.xpath, //current tag path depth
                        z: util.elementTypeMap(object.tagName, object.inputType), //z=1按钮; z=2文字; z=3图片; map, refer to html tag and input type
                        y3: xpathArr.join('/'),
                        y4: md5(util.encodeURIExceptProtocol(location.absUrl)),
                        ur: location.absUrl,
                        u: VERSION, // sdk版本号
                        s2: clientOptions.s2,
                        w2: 1 // w2=1表示应用内H5 页面；w2=0标识客户端原生页面
                    }
                ]
            };
            // Web端日志规范
            dataObj.web = {
                wevent: [
                    {
                        v: eventId, // event ID format: path_text
                        a: globalObject.appId || '', //app ID
                        wt: globalObject.wt || '', // app version
                        d: globalObject.d || '', //uid
                        g: osInfo.os,  // 操作系统
                        t1: dateTime.unixTime(), //进入p1客户端时间
                        w: eventTypeMap(object.eventType), //option type by user
                        x1: object.clientX,
                        y1: object.clientY,
                        y: object.innerText, //contains html text
                        y2: object.xpath, //current tag path depth
                        z: util.elementTypeMap(object.tagName, object.inputType), //z=1按钮; z=2文字; z=3图片; map, refer to html tag and input type
                        y3: xpathArr.join('/'),
                        y4: md5(util.encodeURIExceptProtocol(location.absUrl)),
                        ur: location.absUrl,
                        u: VERSION, // sdk版本
                        s2: cookies.get('s2'), // 会话ID
                        co: cookies.get('co'), // 识别用户
                        p: md5(util.encodeURIExceptProtocol(location.referUrl)),
                        pr: location.referUrl,
                        do: location.domain,
                        po: location.port,
                        pa: location.path,
                        ht: location.hash,
                        qu: location.search,
                        ti: location.title,
                        ua: location.userAgent,
                        w2: 2, // w2=1表示应用内H5 页面；w2=0标识客户端原生页面
                        hr: object.href || ''
                    }
                ]
            };

            logger.c('click/touch',true);
            sendHttpRequest(dataObj);

            // 事件类型 Map
            function eventTypeMap(eventType) {
                eventType = eventType.toLocaleLowerCase();
                var ret = 1; //自定义
                switch (eventType) {
                    case 'click':
                        ret = 1;
                        break;
                    case 'press':
                        ret = 2;
                        break;
                    case 'top':
                        ret = 3;
                        break;
                    case 'bottom':
                        ret = 4;
                        break;
                    case 'left':
                        ret = 5;
                        break;
                    case 'right':
                        ret = 6;
                        break;
                }
                return ret;
            }
        };

        var os = deviceDetector.os().os;
        var _event = 'click';
        if(os === 'Android' || os === 'iOS' || os === 'winPhone'){
            _event = 'touchstart'
        }
        if (globalObject.selection){
            //圈选打开，进入圈选模式
            var selectionCircle = new SelectionCircle();
            eventListener.addEvent(document.body, _event, function (e) {
                handlerWebSelection(e, selectionCircle);
                e.stopPropagation();
                // console.log(jsBridge.getTargetElementForEnd(e.clientX, e.clientY));
                // console.log(jsBridge.getTargetElementForEnd(e.targetTouches[0].clientX, e.targetTouches[0].clientY));
            });

            //media
            // eventListener.addEvent(document.body, 'touchstart', function (e) {
            //     handlerWebSelection(e, selectionCircle);
            //     e.stopPropagation();
            //     // alert(jsBridge.getTargetElementForEnd(e.targetTouches[0].clientX, e.targetTouches[0].clientY));
            // });
            // alert(document.getElementsByTagName('video')[0])
        }else{
            //无埋点模式
            eventListener.addEvent(document.body, _event, handlerWebCrawler);
        }
    };

    function sendPagePathEvent() {
        var osInfo = deviceDetector.os();
        var queryObj = util.queryString();
        /* start log */
        var timeStamp = dateTime.unixTime();
        if (!location.referUrl) {
            sessionStorage.removeItem('t1');
        }

        var dataObj = {};
        /*移动端内嵌*/
        dataObj.client = {
            pagepath: [
                {
                    a: clientOptions.appId || globalObject.appId, // app ID
                    b: clientOptions.b, // app version
                    c: clientOptions.c, // channel ID
                    d: clientOptions.d, // uid
                    e: clientOptions.e, // device ID
                    g: clientOptions.g, // 操作系统
                    j: clientOptions.j, // 广告标识符号
                    t1: clientOptions.t || parseInt(sessionStorage.getItem('t1')) || '', // 进入p2客户端时间
                    t2: timeStamp, // 进入p2客户端时间
                    p: md5(util.encodeURIExceptProtocol(clientOptions.p || location.referUrl)), // 上个页面
                    pr:clientOptions.p || location.referUrl,
                    q:md5(util.encodeURIExceptProtocol(location.absUrl)),//本页面
                    qr: location.absUrl, // p2页面名称（使用控件名）
                    u: VERSION, // sdk版本号
                    s2: clientOptions.s2, // 会话ID
                    w2: 1 // w2=1表示应用内H5 页面；w2=0标识客户端原生页面
                }
            ]
        };
        /*web端嵌入*/
        dataObj.web = {
            wpagepath: [
                {
                    a: globalObject.appId || '', // 应用ID
                    wt: globalObject.wt || '', //网站类型
                    d: globalObject.d || '', // 用户标识,
                    g: osInfo.os,  // 操作系统
                    t1: parseInt(sessionStorage.getItem('t1')) || '', // 进入p2客户端时间
                    t2: timeStamp, // 当前页面的时间
                    p: md5(util.encodeURIExceptProtocol(location.referUrl)), // 上个页面
                    pr: location.referUrl,
                    q:md5(util.encodeURIExceptProtocol(location.absUrl)), // 本页面
                    qr: location.absUrl,
                    u: VERSION, // sdk版本号
                    s2: cookies.get('s2'), // 会话ID
                    co: cookies.get('co'), // js写的cookie 识别用户
                    do: location.domain,
                    po: location.port,
                    pa: location.path,
                    ht: location.hash,
                    qu: location.search,
                    ti: location.title,
                    so: location.referUrl,
                    ua: location.userAgent,
                    w2: 2, // w2=1表示应用内H5 页面；w2=0标识客户端原生页面
                    ai: queryObj.ai || '', //ID 自定义，当前时间戳
                    as: queryObj.as || '',
                    am: queryObj.am || '',
                    an: queryObj.an || '',
                    ac: queryObj.ac || '',
                    at: queryObj.at || ''
                }
            ]
        };
        sessionStorage.setItem('t1', timeStamp);
        logger.c('pagepath',true);
        sendHttpRequest(dataObj);
    }

    /*
     *  外部接口函数
     *  手动埋点
     */
    jsBridge.setEventU = function (jsonObj) {
        //appId, wt, uid, eventId, href, expand
        jsonObj = jsonObj || {};
        var osInfo = deviceDetector.os();

        var dataObj = {};
        dataObj.client = {
            client_eventu: [
                {
                    v1: jsonObj.eventId || '',
                    a: clientOptions.appId || globalObject.appId, //app ID
                    b: clientOptions.b,
                    c: clientOptions.c,
                    d: clientOptions.d || jsonObj.d || globalObject.d || '', //uid
                    e: clientOptions.e,
                    g: clientOptions.g || osInfo.os, // 操作系统
                    j: clientOptions.j,  // 广告标识符号
                    t1: dateTime.unixTime(),
                    x: jsonObj.expand || {},
                    u: VERSION, // sdk版本号
                    s2: clientOptions.s2, //会话ID
                    y4: md5(util.encodeURIExceptProtocol(location.absUrl)),
                    ur: location.absUrl,
                    w2: 1
                }]
        };

        dataObj.web = {
            weventu: [
                {
                    a: globalObject.appId || '', //app ID
                    wt: globalObject.wt || '', //wt=0 官网; wt=1推广链接
                    s2: cookies.get('s2'), //会话ID
                    d: jsonObj.d || globalObject.d || '', //uid
                    g: osInfo.os, //Android/iOS
                    v1: jsonObj.eventId || '',
                    do: location.domain,
                    po: location.port,
                    pa: location.path,
                    ht: location.hash,
                    p: md5(util.encodeURIExceptProtocol(location.referUrl)), // 上个页面
                    pr: location.referUrl,
                    q:md5(util.encodeURIExceptProtocol(location.absUrl)),// 本页面
                    qr: location.absUrl,
                    qu: location.search,
                    co: cookies.get('co'), // 识别用户
                    ti: location.title,
                    hr: jsonObj.href || '',
                    x: jsonObj.expand || {},
                    u: VERSION, // sdk版本号
                    t1: dateTime.unixTime(),
                    ua: location.userAgent
                }]
        };
        logger.c('eventu',true);
        sendHttpRequest(dataObj);
    };

    //TODO: 圈选接口函数，给原生使用
    // 获取当前坐标对应的element
    function getTargetElement(x, y) {
        var targetElement = document.getElementsByTagName('body')[0];
        var maybeEls = [targetElement];
        var zIndex = 0,zIdx = null;
        var excludeEls = {
            '#text' : true,
            '#comment' : true,
            'SCRIPT' : true,
            'BR' : true
        };

        searchElement(targetElement);

        function searchElement(elmt) {
            var child = elmt.children;
            for (var i = 0; i < child.length; i++) {
                var nodeEl = child[i];
                if (!excludeEls[nodeEl.nodeName] && nodeEl.style.display !== 'none' && nodeEl.hidden !== true && !nodeEl.classList.contains('cw-selection-circle')) {
                    var X = nodeEl.getBoundingClientRect().left;
                    var Y = nodeEl.getBoundingClientRect().top;
                    var W = nodeEl.offsetWidth;
                    var H = nodeEl.offsetHeight;

                    if (X <= x && Y <= y && X + W > x && Y + H > y) {
                        maybeEls.push(nodeEl);

                        if(util.css(nodeEl,'position') !== 'static'){
                            var z = util.css(nodeEl,'z-index') === 'auto'? 1 : util.css(nodeEl,'z-index');
                            if(z >= zIndex){
                                zIndex = z;
                                zIdx = maybeEls.length - 1;
                            }
                        }
                    }
                    searchElement(nodeEl);
                }
            }
        }

        maybeEls.push(maybeEls.splice(zIdx, 1)[0]);
        //判断是否为子元素
        var result = [];
        function sort(lastEl) {
            result.push(lastEl);
            for(var j=0;j<maybeEls.length;j++){
                var maybeEl = maybeEls[j];
                for(var k = 0;k < lastEl.children.length;k++){
                    if(lastEl.children[k] === maybeEl){
                        sort(maybeEl);
                        break;
                    }
                }
            }
            return result;
        }
        maybeEls = sort(maybeEls[maybeEls.length - 1]);
        return maybeEls[maybeEls.length - 1];
    }

    jsBridge.getTargetElementForMove = function (x, y) {
        var targetElement = getTargetElement(x, y);
        if (!targetElement) return;
        var ret = {
            x: targetElement.getBoundingClientRect().left < 0 ? 0 : targetElement.getBoundingClientRect().left,
            y: targetElement.getBoundingClientRect().top < 0 ? 0 : targetElement.getBoundingClientRect().top,
            w: targetElement.offsetWidth > window.innerWidth ? window.innerWidth : targetElement.offsetWidth,
            h: targetElement.offsetHeight > window.innerHeight ? window.innerHeight : targetElement.offsetHeight
        };

        if (window.cycenter_bridge) {
            window.cycenter_bridge.hoverNodes(JSON.stringify(ret));
        }else{
            return JSON.stringify(ret);
        }
    };

    jsBridge.getTargetElementForEnd = function (x, y) {
        var ret = getTargetElement(x, y);
        var xpath = util.getXPath(ret);
        var allXpath = util.get3LayersXpath(util.getAllXpath(xpath));
        var viewArray = [];
        var n = 0;

        function getViewArray(dom) {
            if( dom.nodeName === 'HTML' || dom.nodeName === '#document' || !dom) {
              return;
            }
            if (n < 3) {
                var _obj = {
                    path: util.getXPath(dom),
                    x: dom.getBoundingClientRect().left < 0 ? 0 : dom.getBoundingClientRect().left,
                    y: dom.getBoundingClientRect().top < 0 ? 0 : dom.getBoundingClientRect().top,
                    w: dom.offsetWidth > window.innerWidth ? window.innerWidth : dom.offsetWidth,
                    h: dom.offsetHeight > window.innerHeight ? window.innerHeight : dom.offsetHeight,
                    nodeType:util.elementTypeMap(dom.tagName, dom.type),
                    ID: md5(util.getXPath(dom))
                };
                viewArray.push(_obj);
                n++;
                getViewArray(dom.parentNode);
            }
        }

        getViewArray(ret);

        var obj = {
            widgets: allXpath.join('/'),
            viewArray: viewArray
        };
        var retText = util.trim(ret.innerText);
        var limitedTextID = md5(xpath + '_' + retText);
        var unLimitedTextID = md5(xpath);
        obj.viewArray[0].isContainText = obj.isContainText = (retText === '') ? 0 : 1;
        obj.viewArray[0].unLimitedTextID = obj.unLimitedTextID = unLimitedTextID;
        obj.viewArray[0].limitedTextID = obj.limitedTextID = limitedTextID;
        obj.viewArray[0].text = obj.text = retText || ret.title;
        obj.host = window.location.host;
        obj.url = util.encodeURIExceptProtocol(location.absUrl);
        obj.nodeType = util.elementTypeMap(ret.tagName, ret.type);

        if (window.cycenter_bridge) {
            window.cycenter_bridge.saveEvent(JSON.stringify(obj));
        }else{
            return JSON.stringify(obj);
        }
    };

    var getEjsObject = function () {
        var ret = {
        };
        var _ejs = window._ejs;
        for (var i = 0; i < _ejs.length; i++) {
            var key = _ejs[i][0];
            var value = _ejs[i][1];
            if(key.toLowerCase() === 'env'){
                if(value.toString() === 'true' || value.toString() === 'dev'){
                    ret['env'] = 'dev';
                }else{
                    ret['env'] = 'prd';
                }
                continue;
            }else if(key.toLowerCase() === 'platform' && value !== 'client'){
                continue;
            }
            ret[key] = _ejs[i][1];
        }
        return ret;
    };

    var isClientParamsPassed = function () {
        var ret = true;
        if (globalObject.platform === 'client' &&
            clientOptions.appId !== '' &&
            clientOptions.c !== '' &&
            clientOptions.d !== '') {
            ret = true;
        }
        return ret;
    };

    function sendHttpRequest(dataObj) {
        var postData = {};
        if (globalObject.platform === 'client') {
            postData = dataObj.client;
            //客户端模式下，pagepath交给客户端发送，jssdk取消发送
            if(postData.pagepath) return;
            if(postData.pagepath && postData.pagepath[0].p === postData.pagepath[0].q) return;
            if (!isClientParamsPassed()) return; //对客户端H5，基础参数iosOptions没有值，不上报日志
        }
        if (globalObject.platform === 'web') {
            postData = dataObj.web;
        }
        var onSuccess = function (status) {
            logger.c('success=====>');
            logger.c(status, false, true);
            console.groupEnd();
        };
        var onFailure = function (status) {
            logger.c('failure=====>');
            logger.c(status, false, true);
            console.groupEnd();
        };
        logger.c(postData);
        httpRequest.ajax('post', envConfig[globalObject.env].api, JSON.stringify(postData), onSuccess, onFailure);
    }

    /*
     *  initialize globalObject
     */
    globalObject = util.extend(globalObject, getEjsObject());

    /*
     *  application entry, initialization
     */

    //获取系统参数-初始化函数
    /*
     *  设备字段
     * a 应用id
     * b 应用版本号
     * c 渠道id
     * d 用户id
     * e 设备id
     * g 操作系统
     * s2 会话id
     * j 广告id
     */
    var clientOptions = {a: '', b: '', c: '', d: '', e: '', g: '', s2: '', j: ''};
    jsBridge.getAppOptions = function (jsonData) {
        if (jsonData) {
            clientOptions = jsonData;
        }
        // var param = {p: location.absUrl, t: dateTime.unixTime()};
        // sendPagePathEvent();

        if(globalObject.platform === 'client'){
            init();
        }
        var cycenter_bridge = window.cycenter_bridge;
        if (cycenter_bridge) {
            cycenter_bridge.pageChange(JSON.stringify(jsonData));
        }else{
            return JSON.stringify(jsonData);
        }
    };
    var init = function(){
        if (!cookies.isCookieExisted('s2')) {
            cookies.set('s2', S2_TIME);
        }
        if (!cookies.isCookieExisted('co')) {
            cookies.set('co', CO_TIME);
        }
        if(FW_STATE === 'uninstall'){
            addListenerForTags();
            FW_STATE = 'ready';
        }
        //geoInfo.location();
        //geoInfo.ipAddress();
        // if(globalObject.platform === 'web') {
        //     sendPagePathEvent();
        // }
    };

    window.Cysdk = jsBridge;
    //客户端自动注入，客户端调用初始化
    if(globalObject.platform === 'web'){
        //web端按照在load事件触发时初始化。
        eventListener.addEvent(window, 'load', init);
        sendPagePathEvent();
    }else{
        window.webviewJSbridge = window.Cysdk;
    }
})(window);