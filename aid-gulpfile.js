var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var browserify = require('gulp-browserify');
var htmlmin = require('gulp-htmlmin');

var taskMap = ['v1000'];
var ver = '';
for(var i in taskMap){
    if(taskMap.hasOwnProperty(i)){
        ver = taskMap[i];
    }else{
        continue;
    }
    (function (ver) {
        gulp.task(ver, [], function () {
            gulp.src( 'aid/aid' + ver.slice(1) + '.js')
                .pipe(browserify())
                .pipe(uglify())
                .pipe(concat('aid' + ver.slice(1) + '.js'))
                .pipe(gulp.dest('dist/aidsdk'));
            gulp.src('aid/img/*').pipe(gulp.dest('dist/aidsdk/img'));
            var options = {
                removeComments: true,//清除HTML注释
                collapseWhitespace: true,//压缩HTML
                collapseBooleanAttributes: true,//省略布尔属性的值 <input checked="true"/> ==> <input />
                removeEmptyAttributes: true,//删除所有空格作属性值 <input id="" /> ==> <input />
                removeScriptTypeAttributes: true,//删除<script>的type="text/javascript"
                removeStyleLinkTypeAttributes: true,//删除<style>和<link>的type="text/css"
                minifyJS: true,//压缩页面JS
                minifyCSS: true//压缩页面CSS
            };
            gulp.src('aid/aid_getter.html').pipe(htmlmin(options)).pipe(gulp.dest('dist/aidsdk'));
            gulp.src(['aid/*.html','!aid/aid_getter.html']).pipe(gulp.dest('dist/aidsdk'));

            gulp.src( 'aid/libs/*')
                .pipe(uglify())
                .pipe(gulp.dest('dist/aidsdk/libs'))
        });
    })(ver)
}

gulp.task('watch',[],function () {
    gulp.watch('aid/*',['default']);
});

gulp.task('default', taskMap);