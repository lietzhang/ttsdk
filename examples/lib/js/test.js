;(function(factory){
    //JS操作cookies方法!
    //写cookies
    function setCookie(name,value)
    {
        var Days = 30;
        var exp = new Date();
        exp.setTime(exp.getTime() + Days*24*60*60*1000);
        document.cookie = name + "="+ escape (value) + ";path=/;domain=;expires=" + exp.toGMTString();
    }
    function getCookie(name)
    {
        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
        if(arr=document.cookie.match(reg))
            return unescape(arr[2]);
        else
            return null;
    }
    if(getCookie('isdone')){
        return false;
    }else{
        setCookie('isdone','true')
    }
    // document.getElementsByTagName('div')[0].innerText = factory(window);
    // var img = document.createElement('img');
    var img =new Image();
    img.src = '/testaa/testFinger?ua='+ window.navigator.userAgent +'&fp=' + factory(window) + '&v=' + new Date().getTime()

})(function (window, customStr) {
    var navigator = window.navigator;
    var screen = window.screen;
    var document = window.document;
    var customStr = customStr ? customStr : "";

    function bin2hex(s) {
        var i, l, o = '',
            n;

        s += '';

        for (i = 0, l = s.length; i < l; i++) {
            n = s.charCodeAt(i)
                .toString(16);
            o += n.length < 2 ? '0' + n : n;
        }

        return o;
    }

    function getUUID() {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext("2d");

        // detect browser support of canvas winding
        // http://blogs.adobe.com/webplatform/2013/01/30/winding-rules-in-canvas/
        // https://github.com/Modernizr/Modernizr/blob/master/feature-detects/canvas/winding.js
        ctx.rect(0, 0, 10, 10);
        ctx.rect(2, 2, 6, 6);

        ctx.textBaseline = "alphabetic";
        ctx.fillStyle = "#f60";
        ctx.fillRect(125, 1, 62, 20);
        ctx.fillStyle = "#069";
        // https://github.com/Valve/fingerprintjs2/issues/66
        ctx.font = "11pt Arial";
        ctx.fillText("Cwm fjordbank glyphs vext quiz, \ud83d\ude03", 2, 15);
        ctx.fillStyle = "rgba(102, 204, 0, 0.2)";
        ctx.font = "18pt Arial";
        ctx.fillText("Cwm fjordbank glyphs vext quiz, \ud83d\ude03", 4, 45);
        // canvas blending
        // http://blogs.adobe.com/webplatform/2013/01/28/blending-features-in-canvas/
        // http://jsfiddle.net/NDYV8/16/
        ctx.globalCompositeOperation = "multiply";
        ctx.fillStyle = "rgb(255,0,255)";
        ctx.beginPath();
        ctx.arc(50, 50, 50, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fill();
        ctx.fillStyle = "rgb(0,255,255)";
        ctx.beginPath();
        ctx.arc(100, 50, 50, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fill();
        ctx.fillStyle = "rgb(255,255,0)";
        ctx.beginPath();
        ctx.arc(75, 100, 50, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fill();
        ctx.fillStyle = "rgb(255,0,255)";
        // canvas winding
        // http://blogs.adobe.com/webplatform/2013/01/30/winding-rules-in-canvas/
        // http://jsfiddle.net/NDYV8/19/
        ctx.arc(75, 75, 75, 0, Math.PI * 2, true);
        ctx.arc(75, 75, 25, 0, Math.PI * 2, true);
        ctx.fill("evenodd");

        var b64 = canvas.toDataURL().replace("data:image/png;base64,", "");
        //然后用toDataURL方法对生成的canvas图像进行64码进制转换
        // console.log("b64="+b64);
        var bin = atob(b64);
        // console.log(bin);//bin这里是一张图片了，解码图片
        // console.log("bin=16-12"+bin.slice(-16, -12))
        //这里使用js内置的 atob()方法将64进制的解码一下
        var crc = bin2hex(bin.slice(-16, -12));
        // console.log("crc="+crc)
        return bin;

        //     var gpu = document.createElement("canvas").getContext("webgl");
        //     return gpu.getParameter(gpu.VERSION)
    }

    function getDeviceInfoHash() {
        var deviceInfo = '';
        deviceInfo += navigator.appName; //浏览器的正式名称
        deviceInfo += navigator.appCodeName; //与浏览器相关的内部代码名
        deviceInfo += navigator.mimeTypes.length; // 浏览器支持的所有MIME类型的数组
        deviceInfo += navigator.platform; // 浏览器正在运行的操作系统平台，包括Win16(windows3.x)

        var userAgent = navigator.userAgent.toLowerCase();
        var os = 'pc';
        if (userAgent.indexOf('android') > -1 || userAgent.indexOf('linux') > -1) {//安卓手机
            os = 'android';
            os += userAgent.substr(userAgent.indexOf('android') + 8, 3);
        } else if (userAgent.indexOf('iphone') > -1) {//苹果手机
            os = 'ios';
            os += userAgent.match(/cpu iphone os (.+) like mac os/)[1];
        } else if (userAgent.indexOf('windows phone') > -1) {//winphone手机
            os = 'windows';
        }
        deviceInfo += os;
        deviceInfo += navigator.language; //浏览器支持的语言
        deviceInfo += navigator.onLine; //返回浏览器是否处于在线模式
        deviceInfo += navigator.product; // 浏览器的产品名
        deviceInfo += navigator.productSub; //关于浏览器更多信息
        deviceInfo += navigator.vender; // 浏览器厂商名称
        deviceInfo += navigator.vendorSub; // 关于浏览器厂商更多的信息
        for(var idx in navigator.plugins){
            deviceInfo += navigator.plugins[idx].name;
        }
        deviceInfo += screen.colorDepth;
        deviceInfo += screen.height;
        deviceInfo += screen.width;
        deviceInfo += screen.pixelDepth;
        return deviceInfo.toLowerCase().replace(/\s/g, "");
    }
    return md5(getUUID() + getDeviceInfoHash() + customStr)// + '\n\n' + navigator.userAgent.toLowerCase();
});
