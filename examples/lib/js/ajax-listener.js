;(function () {
    var wrapSend = function () {
        console.log('wrap');
        var send;
        var _send = function () {
            console.log('send');
            send.apply(this,arguments);
            var that = this;
            this.addEventListener("readystatechange",function (e) {
                console.log(e)
            });
            this.addEventListener("error",function (e) {
                console.log(e)
            });
        };
        send = window.XMLHttpRequest.prototype.send;
        window.XMLHttpRequest.prototype.send = _send;
    };
    wrapSend()
})()